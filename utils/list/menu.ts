const MENU = {
  HOME: {
    id: 1,
    path: '/',
    name: 'home'
  },
  BROWSESTREETWEAR: {
    id: 2,
    path: '/browse',
    name: 'browse'
  },
  HOWITWORKS: {
    id: 3,
    path: '/how-it-works',
    name: '?scroll=howitworks'
  },
  PLANS: {
    id: 4,
    path: '/plans',
    name: '?scroll=plans'
  },
  SHOP: {
    id: 5,
    path: '/shop',
    name: 'shop'
  },
  ABOUT: {
    id: 6,
    path: '/about-us',
    name: 'about'
  },
  PICKFORME: {
    id: 7,
    path: '/pick-for-me'
  },
  HOWPICKFORMEWORKS: {
    id: 8,
    path: '/how-pick-for-me-works'
  },
  ACCOUNT: {
    id: 9,
    path: '/dashboard'
  },
  CART: {
    id: 10,
    path: '/browse'
  },
  SIGNUP: {
    id: 11,
    path: '/signup'
  },
  SIGNIN: {
    id: 12,
    path: '/login'
  },
  RECOVER: {
    id: 13,
    path: '/recover'
  },
  RESET: {
    id: 14,
    path: '/reset'
  },
  WISHLIST: {
    id: 15,
    path: '/wishlist'
  },
  CHECKOUT: {
    id: 16,
    path: '/checkout'
  },
  TERMS: {
    id: 17,
    path: '/terms-of-use'
  },
  PRIVACY: {
    id: 18,
    path: '/privacy'
  },
  FACEBOOK: {
    id: 19,
    path: 'https://www.facebook.com/hype2u.streetwear/'
  },
  INSTAGRAM: {
    id: 20,
    path: 'https://www.instagram.com/hype2u.streetwear/'
  }
}

const MENUDASHBOARD = {
  HOME: {
    id: 19,
    path: '/dashboard'
  },
  PROFILE: {
    id: 20,
    path: '/dashboard/profile'
  },
  ADDRESS: {
    id: 21,
    path: '/dashboard/address',
    sub: {
      ADDRESSADD: {
        id: 22,
        path: '/dashboard/address/add'
      },
      ADDRESSEDIT: {
        id: 23,
        path: ''
      }
    }
  },
  ORDERS: {
    id: 24,
    path: '/dashboard/orders',
    sub: {
      ORDERSVIEW: {
        id: 25,
        path: ''
      }
    }
  },
  SUBSCRIPTION: {
    id: 26,
    path: '/dashboard/subscription',
    sub: {
      SUBSCRIPTIONEDIT: {
        id: 27,
        path: '/dashboard/subscription/edit'
      }
    }
  },
  PAYMENT: {
    id: 28,
    path: '/dashboard/payment',
    sub: {
      PAYMENTEDIT: {
        id: 29,
        path: '/dashboard/payment/edit'
      }
    }
  },
  CHANGEPASSWORD: {
    id: 30,
    path: '/dashboard/password'
  },
  SIGNOUT: {
    id: 31,
    path: '/logout'
  }
}

const MOBILEMENU = {
  BROWSESTREETWEAR: {
    id: 1,
    path: '/browse'
  },
  HOWITWORKS: {
    id: 2,
    path: '/how-it-works',
    scroll: '.howitwork-section'
  },
  PLANS: {
    id: 3,
    path: '/plans',
    scroll: '.plan-section'
  },
  SHOP: {
    id: 4,
    path: '/shop',
    name: 'shop'
  },
  ABOUT: {
    id: 5,
    path: '/about-us'
  }
}

export default { MENU, MENUDASHBOARD, MOBILEMENU }
