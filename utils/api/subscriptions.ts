import { NuxtAxiosInstance } from '@nuxtjs/axios'

export default class {
    private axios: NuxtAxiosInstance
    private prefix = '/subscriptions'

    constructor(axios: NuxtAxiosInstance) {
        this.axios = axios
    }

    getCoupon(code: string) {
        return this.axios.get(this.prefix + '/' + code)
    }

    subscribe(stripePlan: string, payment: string, coupon: any = null) {
        if (coupon) {
            return this.axios.post(this.prefix, {
                stripe_plan: stripePlan,
                payment,
                coupon,
            })
        } else {
            return this.axios.post(this.prefix, {
                stripe_plan: stripePlan,
                payment,
            })
        }
    }

    upgrade(stripePlan: string) {
        return this.axios.post(this.prefix + '/upgrade', {
            stripe_plan: stripePlan,
        })
    }

    downgrade(stripePlan: string) {
        return this.axios.post(this.prefix + '/downgrade', {
            stripe_plan: stripePlan,
        })
    }

    cancel(id: number) {
        return this.axios.post(this.prefix + '/' + id + '/cancel')
    }
}
