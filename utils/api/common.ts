import { NuxtAxiosInstance } from '@nuxtjs/axios'

export default class {
    private axios: NuxtAxiosInstance
    private prefix = '/'

    constructor(axios: NuxtAxiosInstance) {
        this.axios = axios
    }

    banners() {
        return this.axios.get(this.prefix + 'banners')
    }

    genders() {
        return this.axios.get(this.prefix + 'genders')
    }

    brands() {
        return this.axios.get(this.prefix + 'brands')
    }
}
