import { NuxtAxiosInstance } from '@nuxtjs/axios'

export default class {
    private axios: NuxtAxiosInstance
    private prefix = '/coupons'

    constructor(axios: NuxtAxiosInstance) {
        this.axios = axios
    }


    promo(coupons: string) {
        return this.axios.post(this.prefix + '/validate',{
            code: coupons,
        } )
    }

}
