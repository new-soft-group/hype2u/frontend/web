import { NuxtAxiosInstance } from '@nuxtjs/axios'

export default class {
    private axios: NuxtAxiosInstance
    private prefix = '/checkout'

    constructor(axios: NuxtAxiosInstance) {
        this.axios = axios
    }

    checkout(
        shippingAddress: object,
        currency: string,
        deliveryDate: string,
        deliveryTime: string
    ) {
        return this.axios.post(this.prefix, {
            shipping_address: shippingAddress,
            currency,
            timeslot_date: deliveryDate,
            timeslot: deliveryTime,
        })
    }

    checkoutShop(shippingAddress: object, currency: string, payment: any) {
        return this.axios.post('shop' + this.prefix, {
            shipping_address: shippingAddress,
            currency,
            payment_method: payment,
        })
    }
}
