import { NuxtAxiosInstance } from '@nuxtjs/axios'

export default class {
    private axios: NuxtAxiosInstance
    private prefix = '/holidays'

    constructor(axios: NuxtAxiosInstance) {
        this.axios = axios
    }

    getHolidays() {
        return this.axios.get(this.prefix)
    }
}
