import { NuxtAxiosInstance } from '@nuxtjs/axios'

export default class {
    private axios: NuxtAxiosInstance
    private prefix = '/products'

    constructor(axios: NuxtAxiosInstance) {
        this.axios = axios
    }

    filterable() {
        return this.axios.get(this.prefix + '/filterable')
    }

    product(slug: string) {
        return this.axios.get('product/' + slug)
    }

    getMostPopularProducts() {
        const filter = {
            most_popular: {},
        }
        filter.most_popular = 1

        const data = this.axios.get('products', {
            params: {
                filter,
                itemsPerPage: 10,
            },
        })
        return data
    }

    notifyMe(product: any) {
        return this.axios.post(
            this.prefix + '/stocks/' + product.id + '/notify'
        )
    }

    getProducts(
        btype = [] as any,
        search: string,
        sort: string,
        types = [],
        categories = [],
        brands = [],
        genders = [],
        variants = [],
        price: number,
        page: number = 1
    ) {
        let data
        let sortBy
        let sortDesc
        let newSort: any

        const filter = {
            business_type: {},
            type: {},
            categories: {},
            brand: {},
            gender: {},
            variants: {},
            price: {},
        }

        // Sort
        if (sort) {
            newSort = sort.split('&')
            sortBy = newSort[0]
            sortDesc = newSort[1]
        }

        // Business type
        if (btype.length > 0) {
            const btypeObject = [] as any
            for (let bt = 0; bt < btype.length; bt++) {
                btypeObject.push(btype[bt])
            }
            filter.business_type = btypeObject
        }

        // Types
        if (types.length > 0) {
            const typeObject = []
            for (let t = 0; t < types.length; t++) {
                typeObject.push(types[t])
            }
            filter.type = typeObject
        }

        // Categories
        if (categories.length > 0) {
            const catObject = []
            for (let c = 0; c < categories.length; c++) {
                catObject.push(categories[c])
            }
            filter.categories = catObject
        }

        // Brands
        if (brands.length > 0) {
            const brandObject = []
            for (let b = 0; b < brands.length; b++) {
                brandObject.push(brands[b])
            }
            filter.brand = brandObject
        }

        // Genders
        if (genders.length > 0) {
            const genderObject = []
            for (let g = 0; g < genders.length; g++) {
                genderObject.push(genders[g])
            }
            filter.gender = genderObject
        }

        // Variants
        if (variants.length > 0) {
            const variantObject = []
            for (let v = 0; v < variants.length; v++) {
                variantObject.push(variants[v])
            }
            filter.variants = variantObject
        }

        // Price
        if (price) {
            const priceObject = [0]
            priceObject.push(price)
            filter.price = priceObject
        }

        if (
            search ||
            sortBy ||
            sortDesc ||
            filter.business_type ||
            filter.type ||
            filter.categories ||
            filter.brand ||
            filter.gender ||
            filter.variants ||
            filter.price
        ) {
            data = this.axios.get('products', {
                params: {
                    search,
                    sort_by: sortBy,
                    sort_desc: sortDesc,
                    filter,
                    page,
                },
            })
        } else {
            data = this.axios.get('products')
        }

        return data
    }
}
