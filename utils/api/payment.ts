import { NuxtAxiosInstance } from '@nuxtjs/axios'

export default class {
    private axios: NuxtAxiosInstance
    private prefix = '/payments/methods'

    constructor(axios: NuxtAxiosInstance) {
        this.axios = axios
    }

    default() {
        return this.axios.get(this.prefix + '/default')
    }

    payments() {
        return this.axios.get(this.prefix)
    }

    deletePayment(id: string, userId: number) {
        const data = this.axios.delete(this.prefix + '/' + userId + '/' + id)
        return data
    }
}
