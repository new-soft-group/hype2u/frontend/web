import { NuxtAxiosInstance } from '@nuxtjs/axios'

export default class {
    private axios: NuxtAxiosInstance
    private prefix = '/users'

    constructor(axios: NuxtAxiosInstance) {
        this.axios = axios
    }

    profile() {
        return this.axios.get(this.prefix)
    }

    update(
        firstName: string,
        lastName: string,
        mobile: string,
        gender: number,
        dob: string,
        size: number
    ) {
        return this.axios.post(this.prefix, {
            first_name: firstName,
            last_name: lastName,
            mobile,
            gender_id: gender,
            dob,
            size_id: size,
        })
    }

    changePassword(
        oldPassword: string,
        password: string,
        passwordConfirmation: string
    ) {
        return this.axios.post(this.prefix + '/password', {
            old_password: oldPassword,
            password,
            password_confirmation: passwordConfirmation,
        })
    }

    listAddress() {
        return this.axios.get(this.prefix + '/addresses')
    }

    createAddress(
        firstName: string,
        lastName: string,
        mobile: string,
        company: string,
        address1: string,
        address2: string,
        city: string,
        state: string,
        postcode: string,
        country: string
    ) {
        return this.axios.post(this.prefix + '/addresses', {
            first_name: firstName,
            last_name: lastName,
            mobile,
            company,
            address_1: address1,
            address_2: address2,
            city,
            state,
            postcode,
            country,
        })
    }

    updateAddress(
        id: number,
        firstName: string,
        lastName: string,
        mobile: string,
        company: string,
        address1: string,
        address2: string,
        city: string,
        state: string,
        postcode: string,
        country: string
    ) {
        return this.axios.put(this.prefix + '/addresses/' + id, {
            first_name: firstName,
            last_name: lastName,
            mobile,
            company,
            address_1: address1,
            address_2: address2,
            city,
            state,
            postcode,
            country,
        })
    }

    deleteAddress(id: number) {
        const data = this.axios.delete(this.prefix + '/addresses/' + id)
        return data
    }

    defaultShipping(id: number) {
        return this.axios.put(this.prefix + '/addresses/shipping/' + id)
    }

    defaultBilling(id: number) {
        return this.axios.put(this.prefix + '/addresses/billing/' + id)
    }

    subscriptions() {
        return this.axios.get(this.prefix + '/subscriptions')
    }

    cardSetup() {
        return this.axios.get(this.prefix + '/cards/setup')
    }

    orders(perPage: number = 5, page: number = 1) {
        return this.axios.get(this.prefix + '/orders', {
            params: {
                per_page: perPage,
                page,
            },
        })
    }

    invoices(perPage: number = 5, page: number = 1) {
        return this.axios.get(this.prefix + '/invoices', {
            params: {
                per_page: perPage,
                page,
            },
        })
    }
}
