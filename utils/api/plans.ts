import { NuxtAxiosInstance } from '@nuxtjs/axios'

export default class {
    private axios: NuxtAxiosInstance
    private prefix = '/plans'

    constructor(axios: NuxtAxiosInstance) {
        this.axios = axios
    }

    plans() {
        return this.axios.get(this.prefix)
    }

    plan(id: number) {
        return this.axios.get(this.prefix + '/' + id)
    }
}
