import { NuxtAxiosInstance } from '@nuxtjs/axios'

export default class {
    private axios: NuxtAxiosInstance

    constructor(axios: NuxtAxiosInstance) {
        this.axios = axios
    }

    register(form, form2, sessionId) {
        // form (email, name , password)
        const email = form.email.value
        const name = form.name.value
        const password = form.password.value
        const passwordConfirmation = form.password.value

        // form 2 (firstName, lastName, cc, expiryDate, cvv)
        const firstName = form2.firstName.value || form.name.value
        const lastName = form2.lastName.value || null
        const mobile = form2.mobile.value || null

        let config = {}
        if (sessionId) {
            config = {
                headers: {
                    'X-Cart-Session': sessionId,
                },
            }
        }
        return this.axios.post(
            '/register',
            {
                name,
                first_name: firstName,
                last_name: lastName,
                email,
                mobile,
                password,
                password_confirmation: passwordConfirmation,
            },
            config
        )
    }

    login(email: string, password: string) {
        return this.axios.post('/login', {
            email,
            password,
        })
    }

    recover(email: string, sessionId?: string) {
        let config = {}

        if (sessionId) {
            config = {
                headers: {
                    'X-Cart-Session': sessionId,
                },
            }
        }

        return this.axios.post(
            'forgot/password',
            {
                email,
            },
            config
        )
    }

    resetPassword(
        token: string,
        email: any,
        password: string,
        passwordConfirmation: string
    ) {
        return this.axios.post('reset/password', {
            token,
            email,
            password,
            password_confirmation: passwordConfirmation,
        })
    }
}
