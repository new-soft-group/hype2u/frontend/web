import { NuxtAxiosInstance } from '@nuxtjs/axios'

export default class {
    private axios: NuxtAxiosInstance
    private prefix = '/carts'

    constructor(axios: NuxtAxiosInstance) {
        this.axios = axios
    }

    getCart() {
        return this.axios.get(this.prefix)
    }

    getShopCart() {
        return this.axios.get(this.prefix + '/shop')
    }

    getSurcharges() {
        return this.axios.get(this.prefix + '/surcharges')
    }

    add(
        productId: number,
        productStockId: number,
        productTypeId: number,
        quantity: number,
        businessType: number
    ) {
        if (businessType === 2) {
            return this.axios.post(this.prefix + '/shop', {
                product_id: productId,
                product_stock_id: productStockId,
                product_type_id: productTypeId,
                quantity,
            })
        } else {
            return this.axios.post(this.prefix, {
                product_id: productId,
                product_stock_id: productStockId,
                product_type_id: productTypeId,
                quantity,
            })
        }
    }

    plus(id: number) {
        return this.axios.post(this.prefix + '/shop/items/increment/' + id)
    }

    minus(id: number) {
        return this.axios.post(this.prefix + '/shop/items/decrement/' + id)
    }

    update(id: number) {
        return this.axios.put(this.prefix + '/' + id)
    }

    remove(id: number, type: number) {
        let data
        if (type === 2) {
            data = this.axios.delete(this.prefix + '/shop/' + id)
        } else {
            data = this.axios.delete(this.prefix + '/' + id)
        }
        return data
    }
}
