import { NuxtAxiosInstance } from '@nuxtjs/axios'

export default class {
    private axios: NuxtAxiosInstance
    private prefix = '/orders'

    constructor(axios: NuxtAxiosInstance) {
        this.axios = axios
    }

    getOrder(id: Number) {
        return this.axios.get(this.prefix + '/' + id)
    }
}
