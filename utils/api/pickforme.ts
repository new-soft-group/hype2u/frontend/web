import { NuxtAxiosInstance } from '@nuxtjs/axios'

export default class {
    private axios: NuxtAxiosInstance
    private prefix = '/picks'

    constructor(axios: NuxtAxiosInstance) {
        this.axios = axios
	}

    create(
        genderId: number,
        sizes: [],
        styles: [],
        fits: [],
        brands: [],
        colors: []
    ) {
        return this.axios.post(this.prefix, {
            gender_id:genderId,
            sizes,
			styles,
			fits,
			brands,
			colors
        })
    }

    status() {
        return this.axios.get(this.prefix + '/status')
    }
}
