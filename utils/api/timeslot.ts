import { NuxtAxiosInstance } from '@nuxtjs/axios'

export default class {
    private axios: NuxtAxiosInstance
    private prefix = '/timeslots'

    constructor(axios: NuxtAxiosInstance) {
        this.axios = axios
    }

    getTimeslots() {
        return this.axios.get(this.prefix)
    }

    allocate(orderId: number, date: string, timeslotId: number) {
        return this.axios.post(this.prefix + '/' + timeslotId + '/allocate', {
            order_id: orderId,
            date,
        })
    }
}
