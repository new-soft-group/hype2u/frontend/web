import { NuxtAxiosInstance } from '@nuxtjs/axios'

export default class {
    private axios: NuxtAxiosInstance

    constructor(axios: NuxtAxiosInstance) {
        this.axios = axios
    }

    getWishlist() {
        const data = this.axios.get('wishlists')
        return data
    }

    addWishlist(id: number) {
        const data = this.axios.put('wishlists/' + id)
        return data
    }

    deleteWishlist(id: number) {
        const data = this.axios.delete('wishlists/' + id)
        return data
    }
}
