export default class Address {
    constructor(
        data: any,
        shipping: boolean = false,
        billing: boolean = false
    ) {
        this.id = data.id
        this.firstName = data.first_name
        this.lastName = data.last_name
        this.mobile = data.mobile
        this.company = data.company ?? ''
        this.address1 = data.address_1
        this.address2 = data.address_2
        this.city = data.city
        this.country = data.country
        this.postcode = data.postcode
        this.state = data.state
        this.isShipping = data.shipping
        this.isBilling = data.billing
        this.isShipping = data.shipping ?? shipping
        this.isBilling = data.billing ?? billing
    }

    id: number
    firstName: string
    lastName: string
    mobile: string
    company?: string
    address1: string
    address2: string
    city: string
    country: string
    postcode: string
    state: string
    isShipping: boolean
    isBilling: boolean
}
