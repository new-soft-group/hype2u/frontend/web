export default class CartItem {
    constructor(data: any, type: number) {
        if (type === 1) {
            this.id = data.id
        } else if (type === 2) {
            this.id = data.id
        } else if (type === 3) {
            this.orderId = data.order_id
        } else {
            this.orderId = null
        }
        this.brand = data.product.brand
        this.name = data.product.name
        this.image = data.product.images[0]
        this.status = data.order_status_id
        this.option = data.option
        this.type = data.product.type
        this.price = data.product.pricing
        this.quantity = data.quantity
    }

    id!: number
    orderId: any
    brand: {}
    name: string
    status: number
    image: {}
    option: {}
    type: {}
    price: any = []
    quantity: number
}
