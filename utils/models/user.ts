export default class User {
    constructor(data: any) {
        if (data === null) {
            this.id = 0
            this.firstName = ''
            this.lastName = ''
            this.email = ''
            this.mobile = ''
            this.gender = undefined
            this.dob = ''
            this.size = undefined
            this.shippingId = undefined
            this.billingId = undefined
        } else {
            this.id = data.id
            this.firstName = data.profile.first_name
            this.lastName = data.profile.last_name
            this.email = data.email
            this.mobile = data.profile.mobile ?? ''
            this.gender = data.profile.gender
                ? data.profile.gender.id
                : undefined
            this.dob = data.profile.dob ?? ''
            this.size = data.profile.size ? data.profile.size.id : undefined
            this.shippingId = data.profile.shipping_address
                ? data.profile.shipping_address.id
                : undefined
            this.billingId = data.profile.billing_address
                ? data.profile.billing_address.id
                : undefined
        }
    }

    id: number
    firstName: string
    lastName: string
    email: string
    mobile?: string
    gender?: number
    dob?: string
    size?: number
    shippingId?: number
    billingId?: number
}
