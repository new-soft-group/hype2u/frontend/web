import { Middleware } from '@nuxt/types'

const authenticate: Middleware = ({ app, redirect }) => {
    if (!app.$cookies.get('t')) {
        redirect('/login')
    }
}

export default authenticate
