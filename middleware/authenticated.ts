import { Middleware } from '@nuxt/types'

const authenticated: Middleware = ({ app, redirect }) => {
    if (app.$cookies.get('t')) {
        redirect('/')
    }
}

export default authenticated
