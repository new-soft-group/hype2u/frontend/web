import colors from 'vuetify/es5/util/colors'
import i18n from './locales'

export default {
    server: {
        host: '0.0.0.0',
    },
    /*
     ** Nuxt rendering mode
     ** See https://nuxtjs.org/api/configuration-mode
     */
    // mode: 'universal',
    ssr: true,
    /*
     ** Nuxt target
     ** See https://nuxtjs.org/api/configuration-target
     */
    target: 'server',
    /*
     ** Headers of the page
     ** See https://nuxtjs.org/api/configuration-head
     */
    head: {
        titleTemplate: '%s | ' + process.env.npm_package_name,
        title: process.env.npm_package_name || '',
        meta: [
            {
                charset: 'utf-8',
            },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1',
            },
            {
                hid: 'description',
                name: 'description',
                content:
                    process.env.npm_package_description ||
                    'Hype2U is the first luxury streetwear premium subscription service in the asia. Our experts curate the world of established and emerging streetwear designers and bring the freedom of unlimited style choices to your doorstep.',
            },
        ],
        link: [
            {
                rel: 'icon',
                type: 'image/x-icon',
                href: '/favicon.ico',
            },
        ],
    },
    /*
     ** Global CSS
     */
    css: ['@/assets/scss/main.scss'],
    /*
     ** Plugins to load before mounting the App
     ** https://nuxtjs.org/guide/plugins
     */
    plugins: [
        '~/plugins/axios.ts',
        {
            src: '~/plugins/aos',
            ssr: false,
        },
        {
            src: '~/plugins/owl-carousel',
            ssr: false,
        },
        {
            src: '~/plugins/vue-slider',
            ssr: false,
        },
        { src: '~/plugins/vue-datepicker', ssr: false },
        { src: '~/plugins/draggable', ssr: false },
    ],
    /*
     ** Auto import components
     ** See https://nuxtjs.org/api/configuration-components
     */
    components: true,
    /*
     ** Nuxt.js dev-modules
     */
    buildModules: [
        '@nuxt/typescript-build',
        // Doc: https://github.com/nuxt-community/stylelint-module
        '@nuxtjs/stylelint-module',
        '@nuxtjs/vuetify',
        '@nuxtjs/moment',
    ],
    /*
     ** Nuxt.js modules
     */
    modules: [
        // Doc: https://axios.nuxtjs.org/usage
        '@nuxtjs/axios',
        '@nuxtjs/gtm',
        'cookie-universal-nuxt',
        'nuxt-i18n',
        'nuxt-stripe-module',
        'vue-scrollto/nuxt',
    ],

    gtm: {
        id: process.env.GOOGLE_TAG_MANAGER_ID,
        pageTracking: true,
    },

    i18n: {
        defaultLocale: 'en',
        locales: ['en', 'zh'],
        vueI18n: i18n,
        strategy: 'no_prefix',
    },
    /*
     ** Axios module configuration
     ** See https://axios.nuxtjs.org/options
     */
    axios: {},
    /*
     ** vuetify module configuration
     ** https://github.com/nuxt-community/vuetify-module
     */
    vuetify: {
        customVariables: ['~/assets/variables.scss'],
        theme: {
            dark: true,
            options: {
                customProperties: true,
            },
            themes: {
                dark: {
                    background: '#151515',
                    primary: colors.blue.darken2,
                    accent: colors.grey.darken3,
                    secondary: colors.amber.darken3,
                    info: colors.teal.lighten1,
                    warning: colors.amber.base,
                    error: colors.deepOrange.accent4,
                    success: colors.green.accent3,
                    badge: '#e31f24',
                },
            },
        },
    },
    /*
     ** Build configuration
     ** See https://nuxtjs.org/api/configuration-build/
     */
    build: {},
    env: {
        API_URL: process.env.API_URL,
    },
    stripe: {
        publishableKey: process.env.STRIPE_KEY,
    },
}
