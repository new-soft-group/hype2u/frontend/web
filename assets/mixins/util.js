import {
  parsePhoneNumberFromString
} from 'libphonenumber-js'

export default {
  computed: {
    momentLocale() {
      return this.$store.getters['locale/getLocale'] === 'zh' ? 'zh-cn' : 'en'
    }
  },

  methods: {
    getCountryFlag(code) {
      const country = this.countryLookup.find(c => c.countryCode === code)
      return country ? country.flag : '?'
    },

    getCountryName(code) {
      return this.countryLookup.find(c => c.countryCode === code).name
    },

    randomColor() {
      const num = Math.round(0xFFFFFF * Math.random())
      const r = num >> 16
      const g = num >> 8 & 255
      const b = num & 255

      return 'rgb(' + r + ', ' + g + ', ' + b + ')'
    },

    parsePhone(phone) {
      const _phone = parsePhoneNumberFromString(phone.toString())
      return _phone !== undefined ? _phone : undefined
    },

    normalizePhone(phone, country) {
      const _phone = parsePhoneNumberFromString(phone.toString(), country)
      return _phone !== undefined ? _phone.number : phone
    },

    objectToFormData(object) {
      const formData = new FormData()

      const nested = (object, name, formData) => {
        Object.keys(object).forEach((key) => {
          const value = object[key]

          if (Array.isArray(value)) {
            value.forEach((v, k) => {
              if (v instanceof Object) {
                nested(v, `${name}[${key}][${k}]`, formData)
              } else {
                formData.append(`${name}[${k}]`, v)
              }
            })
          } else if (value instanceof Object && !(value instanceof File)) {
            nested(value, `${name}[${key}]`, formData)
          } else {
            formData.append(`${name}[${key}]`, value)
          }
        })
      }

      Object.keys(object).forEach((key) => {
        const value = object[key]

        if (Array.isArray(value) || value instanceof Object) {
          nested(object[key], `${key}`, formData)
        } else if (['', null, undefined].indexOf(value) !== 1) {
          formData.append(key, value)
        }
      })
      return formData
    },

    locales(item, lang = 'en') {
      return item.find(x => x.lang === lang)
    },

    pricing(item, code = 'MYR') {
      return item.find(x => x.currency_code === code)
    }
  }
}
