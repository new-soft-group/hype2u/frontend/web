# Compile
FROM node:14-alpine
RUN apk add --no-cache git
WORKDIR /app
COPY ./package*.json ./
RUN npm install
COPY . .
RUN rm -rf ./.env.*
ARG env
COPY ./.env.${env} ./.env
RUN npm run build
CMD [ "npm", "start" ]