import { Module, VuexModule, Action, Mutation } from 'vuex-module-decorators'
import { store } from '~/store'

@Module({
    name: 'currency',
    stateFactory: true,
    namespaced: true,
    store,
    dynamic: true,
})
class CurrencyModule extends VuexModule {
    currency: Number = 0 // default
    currencyName: String = 'myr'

    @Action({ commit: 'setCurrency' })
    changeCurrency(currency: Number) {
        return currency
    }

    @Action({ commit: 'setCurrencyName' })
    changeCurrencyName(currencyName: String) {
        return currencyName
    }

    @Mutation
    setCurrency(currency: Number) {
        this.currency = currency
    }

    @Mutation
    setCurrencyName(currencyName: String) {
        this.currencyName = currencyName
    }

    get getCurrency() {
        return this.currency
    }

    get getCurrencyName() {
        return this.currencyName.toUpperCase()
    }
}

export default CurrencyModule
