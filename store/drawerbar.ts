import { Module, VuexModule, Mutation } from 'vuex-module-decorators'
import { store } from '~/store'

@Module({
    name: 'drawerbar',
    stateFactory: true,
    namespaced: true,
    store,
    dynamic: true,
})
class DrawerBarModule extends VuexModule {
    menu: Boolean = false
    filter: Boolean = false
    cart: Boolean = false

    @Mutation
    setMenuDrawer(menu: Boolean) {
        this.menu = menu
    }

    @Mutation
    setFilterDrawer(filter: Boolean) {
        this.filter = filter
    }

    @Mutation
    setCartDrawer(cart: Boolean) {
        this.cart = cart
    }

    get getMenuDrawer() {
        return this.menu
    }

    get getFilterDrawer() {
        return this.filter
    }

    get getCartDrawer() {
        return this.cart
    }
}

export default DrawerBarModule
