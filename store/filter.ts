import { Module, VuexModule, Mutation } from 'vuex-module-decorators'
import { store } from '~/store'

@Module({
    name: 'filter',
    stateFactory: true,
    namespaced: true,
    store,
    dynamic: true,
})
class FilterModule extends VuexModule {
    types = []
    categories = []
    brands = []
    genders = []
    variants = []
    filterSearch: string = ''
    filterSort: string = ''
    filterType = []
    filterCategory = []
    filterBrand = []
    filterGender = []
    filterVariant = []
    filterPrice: string = '' // default
    filterPage: Number = 1

    @Mutation
    setTypesMutation(responseData: []) {
        this.types = responseData
    }

    @Mutation
    setCategoriesMutation(responseData: []) {
        this.categories = responseData
    }

    @Mutation
    setBrandsMutation(responseData: []) {
        this.brands = responseData
    }

    @Mutation
    setGendersMutation(responseData: []) {
        this.genders = responseData
    }

    @Mutation
    setVariantsMutation(responseData: []) {
        this.variants = responseData
    }

    @Mutation
    setFilterSearchMutation(search: string) {
        if (search) {
            // assign to search
            this.filterSearch = search
        }
    }

    @Mutation
    setFilterPageMutation(filter: Number) {
        if (filter) {
            // assign to page
            this.filterPage = filter
        }
    }

    @Mutation
    setFilterSortMutation(filter: string) {
        if (filter) {
            // assign to sort
            this.filterSort = filter
        }
    }

    @Mutation
    setFilterTypeMutation(filter: []) {
        if (filter) {
            // assign to type array
            this.filterType = [...filter]
        }
    }

    @Mutation
    setFilterCategoryMutation(filter: []) {
        if (filter) {
            // assign to categories array
            this.filterCategory = [...filter]
        }
    }

    @Mutation
    setFilterBrandMutation(filter: []) {
        if (filter) {
            // assign to brand array
            this.filterBrand = [...filter]
        }
    }

    @Mutation
    setFilterGenderMutation(filter: []) {
        if (filter) {
            // assign to gender array
            this.filterGender = [...filter]
        }
    }

    @Mutation
    setFilterVariantMutation(filter: []) {
        // assign to variant array
        this.filterVariant = [...filter]
    }

    @Mutation
    setFilterPriceMutation(filter: string) {
        if (filter) {
            // assign to price array
            this.filterPrice = filter
        }
    }

    @Mutation
    clearFilters() {
        this.filterSearch = ''
        this.filterSort = ''
        this.filterType = []
        this.filterCategory = []
        this.filterBrand = []
        this.filterGender = []
        this.filterVariant = []
        this.filterPrice = ''
        this.filterPage = 1
    }

    get getTypes() {
        return this.types
    }

    get getPage() {
        return this.filterPage
    }

    get getCategories() {
        return this.categories
    }

    get getBrands() {
        return this.brands
    }

    get getGenders() {
        return this.genders
    }

    get getVariants() {
        return this.variants
    }

    get getFilter() {
        return (type: string) => {
            if (type === 'search') {
                return this.filterSearch
            }

            if (type === 'page') {
                return this.filterPage
            }

            if (type === 'sort') {
                return this.filterSort
            }

            if (type === 'type') {
                return this.filterType
            }

            if (type === 'category') {
                return this.filterCategory
            }

            if (type === 'brand') {
                return this.filterBrand
            }

            if (type === 'variant') {
                return this.filterVariant
            }

            if (type === 'price') {
                return this.filterPrice
            }
        }
    }
}

export default FilterModule
