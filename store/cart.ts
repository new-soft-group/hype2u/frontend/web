import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import CartItem from '~/utils/models/cartItem'
import { store } from '~/store'

@Module({
    name: 'cart',
    stateFactory: true,
    namespaced: true,
    store,
    dynamic: true,
})
export default class CartModule extends VuexModule {
    cart: CartItem[] = []
    cartCF: CartItem[] = []
    cartShop: CartItem[] = []
    cartPrice: Number = 0
    cartShopPrice: Number = 0
    stripeClientSecret: string = ''
    surcharges: any = 0
    currentOrder: CartItem[] = []

    @Action
    addToCart(product: any) {
        // this.context.commit('updateCartMutation', product.items.map((item: any) => { return new CartItem(item) }))
        this.context.commit('setCart', new CartItem(product, 1))
    }

    @Action
    addToCartShop(product: any) {
        // this.context.commit('updateCartMutation', product.items.map((item: any) => { return new CartItem(item) }))
        this.context.commit('setCartShop', new CartItem(product, 1))
    }

    @Action
    updateCart(product: any) {
        if ('carry_forward' in product) {
            this.context.commit(
                'updateCartCFMutation',
                product.carry_forward.map((item: any) => {
                    return new CartItem(item, 3)
                })
            )
        }
        this.context.commit(
            'updateCartMutation',
            product.items.map((item: any) => {
                return new CartItem(item, 2)
            })
        )
    }

    @Action
    updateCartShop(product: any) {
        this.context.commit(
            'updateCartShopMutation',
            product.items.map((item: any) => {
                return new CartItem(item, 2)
            })
        )
    }

    @Action
    updateSurcharges(surcharge: any) {
        this.context.commit('updateSurchargesMutation', surcharge)
    }

    @Action
    removeFromCart(product: any) {
        this.context.commit('removeCart', product)
    }

    @Action
    removeFromShopCart(product: any) {
        this.context.commit('removeShopCart', product)
    }

    @Action
    addShopItemQty(id: number) {
        this.context.commit('addToShopItemQty', id)
    }

    @Action
    minusShopItemQty(id: number) {
        this.context.commit('minusFromShopItemQty', id)
    }

    @Mutation
    setCart(product: any) {
        // check cart unique array
        const index = this.cart.find((item) => item.id === product.id)

        if (index === undefined) {
            // assign to cart array
            this.cart.push(product)

            // update cart price
            // this.cartPrice += product.price
        } else {
            return 'failed'
        }
    }

    @Mutation
    setCartShop(product: any) {
        // check cart unique array
        let index = this.cartShop.findIndex((item) => item.id === product.id)

        if (index !== undefined) {
            if (index < 0) {
                index = 0
            }
            // assign to cart array
            this.cartShop[index] = product
            // update cart price
            // this.cartPrice += product.price
        } else {
            return 'failed'
        }
    }

    @Mutation
    updateCartMutation(products: CartItem[]) {
        this.cart = products
    }

    @Mutation
    updateCartCFMutation(products: CartItem[]) {
        products.reverse()
        this.cartCF = products
    }

    @Mutation
    updateCartShopMutation(products: CartItem[]) {
        this.cartShop = products
    }

    @Mutation
    updateSurchargesMutation(surcharge: any) {
        this.surcharges = surcharge.surcharges
    }

    @Mutation
    removeCart(product: any) {
        // remove from cart array
        const index = this.cart.findIndex((item) => item.id === product)
        this.cart.splice(index, 1)

        // update cart price
        this.cartPrice = <number>this.cartPrice - product.price
    }

    @Mutation
    removeShopCart(product: any) {
        // remove from cart array
        const index = this.cartShop.findIndex((item) => item.id === product)
        this.cartShop.splice(index, 1)

        // update cart price
        // this.cartShopPrice = <number>this.cartShopPrice - product.price
    }

    @Mutation
    addToShopItemQty(id: number) {
        // add to cart quantity
        const index = this.cartShop.findIndex((item) => item.id === id)

        this.cartShop[index].quantity = this.cartShop[index].quantity + 1

        // update cart price
        this.cartShopPrice =
            <number>this.cartShopPrice + this.cartShop[index].price[0].price
    }

    @Mutation
    minusFromShopItemQty(id: number) {
        // minus from cart quantity
        const index = this.cartShop.findIndex((item) => item.id === id)

        // remove from cart array
        if (this.cartShop[index].quantity - 1 === 0) {
            this.cartShop.splice(index, 1)
        } else {
            this.cartShop[index].quantity = this.cartShop[index].quantity - 1
            // update cart price
            this.cartShopPrice =
                <number>this.cartShopPrice - this.cartShop[index].price[0].price
        }
    }

    @Mutation
    setStripeClientSecret(responseData: any) {
        this.stripeClientSecret = responseData.secret.client_secret
    }

    @Mutation
    clearCart() {
        this.cart = []
        this.cartCF = []
    }

    @Mutation
    clearCartShop() {
        this.cartShop = []
    }

    get getCartItems() {
        return this.cart
    }

    get getCartItemsCF() {
        return this.cartCF
    }

    get getCartShopItems() {
        return this.cartShop
    }

    get getSurcharges() {
        return this.surcharges
    }

    get getCartPrice() {
        return (currency: number) => {
            let totalPrice = 0
            if (this.cart.length > 0) {
                for (let c = 0; c < this.cart.length; c++) {
                    totalPrice += this.cart[c].price[currency].price
                }
            }
            return totalPrice
        }
    }

    get getCartShopPrice() {
        return (currency: number) => {
            let totalShopPrice = 0
            if (this.cartShop.length > 0) {
                for (let c = 0; c < this.cartShop.length; c++) {
                    totalShopPrice +=
                        this.cartShop[c].price[currency].price *
                        this.cartShop[c].quantity
                }
            }
            return totalShopPrice
        }
    }

    get getRetailValue() {
        return (planAmount: number, currency: number) => {
            let totalPrice = 0
            if (this.cart.length > 0) {
                for (let c = 0; c < this.cart.length; c++) {
                    totalPrice += this.cart[c].price[currency].price
                }
            }

            return Math.round((totalPrice / planAmount) * 100)
        }
    }

    get getCurrentOrder() {
        return this.currentOrder
    }

    get getStripeClientSecret() {
        return this.stripeClientSecret
    }
}
