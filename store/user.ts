import { Module, VuexModule, Action, Mutation } from 'vuex-module-decorators'
import Vue from 'vue'
import { throttle } from 'lodash-decorators'
import { store } from '~/store'

// Models
import User from '~/utils/models/user'
import Address from '~/utils/models/address'

@Module({
    name: 'user',
    stateFactory: true,
    namespaced: true,
    store,
    dynamic: true,
})
class UserModule extends VuexModule {
    user: User = new User(null)
    addresses: Address[] = []
    editAddress!: Number

    @Action
    setAddress(addresses: any) {
        this.context.commit('setAddAddress', new Address(addresses))
    }

    @Action
    setUpdateAddress(addresses: any) {
        this.context.commit('setUpdateAddressMutation', new Address(addresses))
    }

    @Action
    setEditAddress(id: Number) {
        this.context.commit('setEditAddressMutation', id)
    }

    @Action
    handleAddressApi(responseData: any) {
        this.context.commit('setAddresses', responseData.data)
    }

    @Action
    setShipping(id: Number) {
        this.context.commit('setShippingAddressMutation', id)
    }

    @Action
    setBilling(id: Number) {
        this.context.commit('setBillingAddressMutation', id)
    }

    @Mutation
    setUser(responseData: any) {
        this.user = new User(responseData)
    }

    @Mutation
    setUserFirstName(firstName: string) {
        Vue.set(this.user, 'firstName', firstName)
    }

    @Mutation
    setUserLastName(lastName: string) {
        Vue.set(this.user, 'lastName', lastName)
    }

    @Mutation
    setUserMobile(mobile: string) {
        Vue.set(this.user, 'mobile', mobile)
    }

    @Mutation
    setUserGender(gender: number) {
        Vue.set(this.user, 'gender', gender)
    }

    @Mutation
    setUserDob(dob: number) {
        Vue.set(this.user, 'dob', dob)
    }

    @Mutation
    setUserSize(size: number) {
        Vue.set(this.user, 'size', size)
    }

    @Mutation
    setAddresses(addresses: Address[]) {
        const newAddresses = addresses.map((address: Address) => {
            let shipping: boolean = false
            let billing: boolean = false
            if (this.user.shippingId === address.id) {
                shipping = true
            }
            if (this.user.billingId === address.id) {
                billing = true
            }

            return new Address(address, shipping, billing)
        })

        this.addresses = newAddresses
    }

    @Mutation
    setAddAddress(address: Address) {
        this.addresses.push(address)
    }

    @Mutation
    setEditAddressMutation(id: Number) {
        this.editAddress = id
    }

    @Mutation
    setUpdateAddressMutation(updatedAddress: Address) {
        const addressIndex = this.addresses.findIndex(
            (address) => address.id === updatedAddress.id
        )
        Vue.set(this.addresses, addressIndex, updatedAddress)
    }

    @Mutation
    setShippingAddressMutation(id: Number) {
        const addressIndex = this.addresses.findIndex(
            (address) => address.id === id
        )
        Vue.set(this.addresses[addressIndex], 'isShipping', true)
    }

    @Mutation
    setBillingAddressMutation(id: Number) {
        const addressIndex = this.addresses.findIndex(
            (address) => address.id === id
        )
        Vue.set(this.addresses[addressIndex], 'isBilling', true)
    }

    @Mutation
    clearUser() {
        this.user = new User(null)
    }

    @Mutation
    deleteAddress(id: number) {
        const addressIndex = this.addresses.findIndex(
            (address) => address.id === id
        )
        this.addresses.splice(addressIndex, 1)
    }

    @Mutation
    clearAddresses() {
        this.addresses = []
    }

    get getUser() {
        return this.user
    }

    get getProfilePercentage() {
        let percentage = 0
        if (this.user.firstName) {
            percentage += 15
        }
        if (this.user.lastName) {
            percentage += 15
        }
        if (this.user.email) {
            percentage += 15
        }
        if (this.user.mobile) {
            percentage += 15
        }
        if (this.user.gender) {
            percentage += 15
        }
        if (this.user.dob) {
            percentage += 15
        }
        if (this.user.size) {
            percentage += 15
        }
        if (percentage > 100) {
            percentage = 100
        }
        return percentage
    }

    get getAddresses() {
        return this.addresses
    }

    get getEditAddress() {
        return this.addresses.find((address) => address.id === this.editAddress)
    }

    get getShippingAddress() {
        let shippingAddress
        if (this.addresses.length > 0) {
            shippingAddress = this.addresses.find(
                (address) => address.isShipping === true
            )

            if (!shippingAddress && this.addresses.length > 0) {
                shippingAddress = this.addresses[0]
            }

            return shippingAddress
        } else {
            return []
        }
    }

    get getBillingAddress() {
        return this.addresses.find((address) => address.isBilling === true)
    }

    get getSelectedShippingAddress() {
        return (id: number) => {
            return this.addresses.find((address) => address.id === id)
        }
    }
}

export default UserModule
