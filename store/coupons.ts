import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import { store } from '~/store'

@Module({
    name: 'coupons',
    stateFactory: true,
    namespaced: true,
    store,
    dynamic: true,
})
class CouponsModule extends VuexModule {
    code: string = "" // default

/*     get getCouponsPlan() {
        return this.code
    } */

    @Mutation
    setCouponsMutation(code: any) {
        this.code = code.coupons

    }
}

export default CouponsModule
