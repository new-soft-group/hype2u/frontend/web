import { Module, VuexModule, Action, Mutation } from 'vuex-module-decorators'
import { store } from '~/store'

@Module({
    name: 'snackbar',
    stateFactory: true,
    namespaced: true,
    store,
    dynamic: true,
})
class SnackBarModule extends VuexModule {
    text: String = ''
    color: String = ''
    icon: String = ''
    isOpen = false

    @Action
    displaySnackBar(value: { text: string; color: string; icon: string }) {
        this.context.commit('setText', value.text)
        this.context.commit('setColor', value.color)
        this.context.commit('setIcon', value.icon)
        this.context.commit('setIsOpen', true)
    }

    @Mutation
    setText(text: string) {
        this.text = text
    }

    @Mutation
    setColor(color: String) {
        this.color = color
    }

    @Mutation
    setIcon(icon: String) {
        this.icon = icon
    }

    @Mutation
    setIsOpen(value: boolean) {
        this.isOpen = value
    }

    get getText() {
        return this.text
    }

    get getColor() {
        return this.color
    }

    get getIcon() {
        return this.icon
    }

    get getIsOpen() {
        return this.isOpen
    }
}

export default SnackBarModule
