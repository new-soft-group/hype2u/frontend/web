import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import { store } from '~/store'

@Module({
    name: 'subscription',
    stateFactory: true,
    namespaced: true,
    store,
    dynamic: true,
})
class SubscriptionModule extends VuexModule {
    // plan: String = 'plan_IOoq9kYJSFez7Z' // default
    plan!: String // default
    planId: Number = 1 // default
    planDetails: {} = {}

    @Action
    setPlan(plan: any) {
        this.context.commit('setPlanMutation', plan)
    }

    @Action
    setUpdatePlan(plan: any) {
        this.context.commit('setUpdatePlanMutation', plan)
    }

    @Mutation
    setPlanMutation(plan: any) {
        this.plan = plan.stripe_plan
        this.planDetails = plan.plan
    }

    @Mutation
    setUpdatePlanMutation(plan: any) {
        this.plan = plan.stripe_plan
        this.planDetails = plan
    }

    @Mutation
    clearSubscription() {
        this.plan = ''
        this.planDetails = {}
    }

    get getPlan() {
        return this.plan
    }

    get getPlanId() {
        return this.planId
    }

    get getPlanDetails() {
        return this.planDetails
    }
}

export default SubscriptionModule
