import { Module, VuexModule, Action, Mutation } from 'vuex-module-decorators'
import { store } from '~/store'
@Module({
    name: 'language',
    stateFactory: true,
    namespaced: true,
    store,
    dynamic: true,
})
class LanguageModule extends VuexModule {
    language: Number = 1 // default
    languageName: String = ''

    @Action({ commit: 'setLanguage' })
    changeLanguage(language: Number) {
        return language
    }

    @Action({ commit: 'setLanguageName' })
    changeLanguageName(languageName: String) {
        return languageName
    }

    @Mutation
    setLanguage(language: Number) {
        this.language = language
    }

    @Mutation
    setLanguageName(languageName: String) {
        this.languageName = languageName
    }

    get getLanguage() {
        return this.language
    }

    get getLanguageName() {
        return this.languageName
    }
}

export default LanguageModule
