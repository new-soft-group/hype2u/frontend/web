import { Module, VuexModule, Action, Mutation } from 'vuex-module-decorators'
import { store } from '~/store'

@Module({
    name: 'dialogbar',
    store,
    dynamic: true,
    stateFactory: true,
    namespaced: true,
})
class DialogBarModule extends VuexModule {
    text: String = ''
    color: String = ''
    products: String = ''
    name: String = ''
    sizes = []
    image?: String = ''
    type!: Number // type 1 = cancel plan
    businessType!: Number
    isOpen: Boolean = false
    isClose: Boolean = false

    @Action
    displayDialogBar(value: {
        text: string
        color: string
        products: string
        name: string
        type: number
        image: string
        businessType: number
    }) {
        this.context.commit('setText', value.text)
        this.context.commit('setColor', value.color)
        this.context.commit('setProducts', value.products)
        this.context.commit('setType', value.type)
        this.context.commit('setImage', value.image)
        this.context.commit('setName', value.name)
        this.context.commit('setBusinessType', value.businessType)
        this.context.commit('setIsOpen', true)
        this.context.commit('setIsClose', true)
    }

    @Mutation
    setText(text: String) {
        this.text = text
    }

    @Mutation
    setColor(color: String) {
        this.color = color
    }

    @Mutation
    setProducts(products: String) {
        this.products = products
    }

    @Mutation
    setName(name: String) {
        this.name = name
    }

    @Mutation
    setType(type: Number) {
        this.type = type
    }

    @Mutation
    setSizes(sizes: []) {
        this.sizes = sizes
    }

    @Mutation
    setBusinessType(type: Number) {
        this.businessType = type
    }

    @Mutation
    setImage(image: string) {
        this.image = image
    }

    @Mutation
    setIsOpen(value: Boolean) {
        this.isOpen = value
    }

    @Mutation
    setIsClose(value: Boolean) {
        this.isClose = value
    }

    get getText() {
        return this.text
    }

    get getName() {
        return this.name
    }

    get getColor() {
        return this.color
    }

    get getProducts() {
        return this.products
    }

    get getType() {
        return this.type
    }

    get getBusinessType() {
        return this.businessType
    }

    get getSizes() {
        return this.sizes
    }

    get getImage() {
        return this.image
    }

    get getIsOpen() {
        return this.isOpen
    }

    get getIsClose() {
        return this.isClose
    }
}

export default DialogBarModule
