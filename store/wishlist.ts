import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import CartItem from '~/utils/models/cartItem'
import { store } from '~/store'

@Module({
    name: 'wishlist',
    stateFactory: true,
    namespaced: true,
    store,
    dynamic: true,
})
class WishlistModule extends VuexModule {
    wishlist: any = []

    @Action({ commit: 'setWishlistMutation' })
    updateWishlist(wishlist: []) {
        return wishlist
    }

    @Action
    addToWishlist(product: any) {
        this.context.commit('addWishlistMutation', product)
    }

    @Action
    removeFromWishlist(id: number) {
        this.context.commit('removeWishlistMutation', id)
    }

    @Mutation
    setWishlistMutation(wishlist: []) {
        this.wishlist = wishlist
    }

    @Mutation
    addWishlistMutation(wishlist: []) {
        this.wishlist.items.push(wishlist)
    }

    @Mutation
    removeWishlistMutation(id: number) {
        for (let i = 0; i < this.wishlist.items.length; i++) {
            if (this.wishlist.items[i].product.id === id) {
                // splice(index, quantity)
                this.wishlist.items.splice(i, 1)
            }
        }
    }

    @Mutation
    clearWishlist() {
        this.wishlist = []
    }

    get getWishlist() {
        return this.wishlist
    }

    get checkWishlist() {
        return (id: number) => {
            const hasItemsProperty = Object.prototype.hasOwnProperty.call(
                this.wishlist,
                'items'
            )

            if (hasItemsProperty) {
                if (this.wishlist.items.length > 0) {
                    for (let i = 0; i < this.wishlist.items.length; i++) {
                        if (this.wishlist.items[i].product.id === id) {
                            return 'mdi-heart'
                        }
                    }
                }
            }

            return 'mdi-heart-outline'
        }
    }
}

export default WishlistModule
