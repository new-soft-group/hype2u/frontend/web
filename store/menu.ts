import { Module, VuexModule, Action, Mutation } from 'vuex-module-decorators'
import Menu from '~/utils/list/menu'
import { store } from '~/store'

@Module({
    name: 'menu',
    stateFactory: true,
    namespaced: true,
    store,
    dynamic: true,
})
class MenuModule extends VuexModule {
    menu: {} = Menu.MENU
    dashboard: {} = Menu.MENUDASHBOARD
    mobile: {} = Menu.MOBILEMENU
    selectedDashboardMenu: Number = 0 // default

    @Action
    selectDashboardMenu(menu: Number) {
        this.context.commit('setDashboardMenu', menu)
    }

    @Mutation
    setDashboardMenu(menu: Number) {
        this.selectedDashboardMenu = menu
    }

    get getMenu() {
        return this.menu
    }

    get getMenuDashboard() {
        return this.dashboard
    }

    get getMobileMenu() {
        return this.mobile
    }

    get getSelectedDashboardMenu() {
        return this.selectedDashboardMenu
    }
}

export default MenuModule
