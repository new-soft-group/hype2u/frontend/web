import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import { store } from '~/store'

@Module({
    name: 'plan',
    stateFactory: true,
    namespaced: true,
    store,
    dynamic: true,
})
class PlanModule extends VuexModule {
    plan: number = 1 // default

    get getSubscribedPlan() {
        return this.plan
    }
}

export default PlanModule
