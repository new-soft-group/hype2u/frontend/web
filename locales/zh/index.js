import meta from './meta.json'
import appBar from './appBar.json'
import common from './common.json'
import buttons from './buttons.json'
import notification from './notification.json'
import form from './form.json'
import signUp from './signUp.json'
import home from './home.json'
import about from './about.json'
import product from './product.json'
import cart from './cart.json'
import dashboard from './dashboard.json'
import orders from './orders.json'
import invoices from './invoices.json'
import appFooter from './appFooter.json'
import box from './box.json'
import snackbar from './snackbar.json'
import auth from './auth.json'
import dialogbar from './dialogbar.json'
import pickForMe from './pickForMe.json'

export default {
  meta,
  appBar,
  common,
  buttons,
  notification,
  form,
  signUp,
  home,
  about,
  product,
  cart,
  dashboard,
  orders,
  invoices,
  appFooter,
  snackbar,
  dialogbar,
  box,
  auth,
  pickForMe
}
