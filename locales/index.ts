import en from './en'
import zh from './zh'

export default {
    fallbackLocale: 'en',
    messages: {
        en,
        zh,
    },
    silentTranslationWarn: true,
}
