import { Context } from '@nuxt/types'
import Aos from 'aos'

import 'aos/dist/aos.css'

export default function ({ app }: Context): void {
    // eslint-disable-next-line new-cap
    const newAOS = Aos.init({
        disable: 'phone',
        delay: 500,
        startEvent: 'load',
        once: true,
    })

    app.AOS = newAOS
}
