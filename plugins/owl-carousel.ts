import { Vue } from 'vue-property-decorator'
const OwlCarousel = require('vue-owl-carousel2')

Vue.use(OwlCarousel)
Vue.component('OwlCarousel', OwlCarousel)
