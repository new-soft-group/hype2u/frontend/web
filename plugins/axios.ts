/* eslint-disable import/no-mutable-exports */
import Vue from 'vue'
import { Context } from '@nuxt/types'
import Auth from '~/utils/api/auth'
import Common from '~/utils/api/common'
import Product from '~/utils/api/product'
import Wishlist from '~/utils/api/wishlist'
import User from '~/utils/api/user'
import Cart from '~/utils/api/cart'
import Payment from '~/utils/api/payment'
import Subscription from '~/utils/api/subscriptions'
import Plan from '~/utils/api/plans'
import Timeslot from '~/utils/api/timeslot'
import Checkout from '~/utils/api/checkout'
import Order from '~/utils/api/orders'
import Holiday from '~/utils/api/holidays'
import Coupons from '~/utils/api/coupons'
import Pickforme from '~/utils/api/pickforme'

interface Api {
    auth: Auth
    common: Common
    product: Product
    wishlist: Wishlist
    user: User
    cart: Cart
    payment: Payment
    subscription: Subscription
    plan: Plan
    timeslot: Timeslot
    checkout: Checkout
    order: Order
    holiday: Holiday
    coupons: Coupons
    pickforme: Pickforme
}

// Get env api url
const baseURL = process.env.API_URL!

let api: Api

declare module 'vue/types/vue' {
    interface Vue {
        $api: {
            auth: Auth
            common: Common
            product: Product
            wishlist: Wishlist
            user: User
            cart: Cart
            payment: Payment
            subscription: Subscription
            plan: Plan
            timeslot: Timeslot
            checkout: Checkout
            order: Order
            holiday: Holiday
            coupons: Coupons
            pickforme: Pickforme
        }
    }
}

export default function ({ app, $axios }: Context): void {
    // Set Base URL
    $axios.setBaseURL(baseURL)
    // Set Token
    $axios.setToken(app.$cookies.get('t'), 'Bearer')

    // Interceptors
    $axios.onError((error: any) => {
        if (
            error.response?.status === 401 &&
            error.response.config.url !== '/login'
        ) {
            // Remove token from axios instance
            $axios.setToken(false)

            // Remove token from cookie
            app.$cookies.remove('t')

            // Clear stores

            // Clear user profile

            // Clear addresses

            // Clear wishlist

            // Clear cart

            // Display Snackbar

            // return redirect('/')
        }

        /* if (error.response?.status === 400) {
            const val = {
                text: error.response.data.message,
                color: 'red',
                icon: 'mdi-close-circle',
            }
            store.dispatch('snackbar/displaySnackBar', val)
        } */
    })

    api = {
        auth: new Auth($axios),
        common: new Common($axios),
        product: new Product($axios),
        wishlist: new Wishlist($axios),
        user: new User($axios),
        cart: new Cart($axios),
        payment: new Payment($axios),
        subscription: new Subscription($axios),
        plan: new Plan($axios),
        timeslot: new Timeslot($axios),
        checkout: new Checkout($axios),
        order: new Order($axios),
        holiday: new Holiday($axios),
        coupons: new Coupons($axios),
        pickforme: new Pickforme($axios)
    }

    Vue.prototype.$api = api
}

export { api }
